import watch from './watch';

watch({
	root: '/var/www/force-file-structure',
	modes: ['warn', 'delete', 'create'],

	rules: {
		'': {
			allowedTypes: ['ts', 'json', 'md'],
		},

		'src': {
			allowedTypes: ['ts', 'js'],
		},

		'dist': {
			allowedTypes: ['ts', 'js'],
		},
	},
});

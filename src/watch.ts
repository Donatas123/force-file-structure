import {watch} from 'chokidar';
import {unlinkSync, rmdirSync, lstatSync, mkdirSync, existsSync} from 'fs';

type Modes = 'warn' | 'delete' | 'create';

type Rules = {
	[key: string]: Rule;
}

type Rule = {
	allowedTypes: string[];
	isRecursive?: boolean;
};

type Config = {
	modes?: Modes[];
	root: string;
	rules: Rules;
};

const folderRules: Rules = {};

export default function (config: Config) {
	if (!config.modes) {
		config.modes = ['warn'];
	}

	if (!config) {
		console.log('No watch config given');

		return;
	}

	const watchOptions = {
		ignored: [
			/node_modules/,
			/(^|[\/\\])\../,
		],
	};

	const isModeEnabled = (mode: Modes) => {
		return config.modes && config.modes.includes(mode);
	};

	const createMissingFolders = () => {
		Object.entries(folderRules).forEach(([folderPath]) => {
			if (!existsSync(folderPath)) {
				if (isModeEnabled('warn')) {
					console.log(`Creating missing folder ${folderPath}`);
				}

				if (isModeEnabled('create')) {
					mkdirSync(folderPath, {recursive: true});
				}
			}
		});
	};

	Object.entries(config.rules).forEach(([key, rule]) => {
		folderRules[`${config.root}/${key}`] = rule;
	});

	createMissingFolders();

	if (folderRules[`${config.root}/`]) {
		folderRules[config.root] = folderRules[`${config.root}/`];
		delete folderRules[`${config.root}/`];
	}

	let depth: number;

	const getRuleByPath = (path: string): Rule | null => {
		depth++;

		if (!path) {
			const rootRule = folderRules[`${config.root}/`];

			return rootRule ? rootRule : null;
		}

		let rule = folderRules[path];

		if (rule) {
			return rule;
		} else {
			const newPathToLookup = path.split('/');

			newPathToLookup.pop();

			return getRuleByPath(newPathToLookup.join('/'));
		}
	};

	const getRule = (path: string) => {
		depth = 0;

		const rule = getRuleByPath(path);

		return {
			isDeep: depth > 2,
			rule,
		};
	};

	const removeFileOrFolder = (path: string) => {
		const isRemovingFolder = lstatSync(path).isDirectory();

		if (isModeEnabled('warn')) {
			console.log(`Removing ${isRemovingFolder ? 'folder' : 'file'} ${path}`);
		}

		if (isModeEnabled('delete')) {
			isRemovingFolder
				? rmdirSync(path, {
					recursive: true,
				})
				: unlinkSync(path);
		}
	};

	const handleFileChange = (path: string) => {
		const {isDeep, rule} = getRule(path);
		const fileType = path.split('.').pop() || '';

		if (isDeep) {
			if (!rule?.isRecursive || (rule.isRecursive && !rule?.allowedTypes.includes(fileType))) {
				removeFileOrFolder(path);
			}
		} else {
			if (!rule?.allowedTypes.includes(fileType)) {
				removeFileOrFolder(path);
			}
		}
	};

	const handleFolderChange = (path: string) => {
		const {rule} = getRule(path);

		if (!folderRules[path]) {
			if (!rule?.isRecursive) {
				removeFileOrFolder(path);
			}
		}
	};

	watch(config.root, watchOptions)
		.on('add', handleFileChange)
		.on('change', handleFileChange)
		.on('addDir', handleFolderChange);
}

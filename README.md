# About

A simple node.js package that allows you to force a specific folder/file structure in a given folder while also forcing
only specific file types to be used in a given folder.

# Installation

`npm install --save-dev force-file-structure`

# Example usage

```javascript
import watch from 'force-folder-structure';

watch({
    root: '/var/www/some-project', // Absolute path where watcher should look for changes
    modes: [
        'warn', // Prints delete/create actions to console
        'delete', // Enables automatic folder/file deletion
        'create', // Enables automatic missing folder creation
    ],    
    rules: {
        // rule for project root directory
        '': {
            allowedTypes: ['json'], // Only .json files are allowed 
        },

        // rule for /images
        'images': {
            allowedTypes: ['png', 'jpg', 'svg'],
            isRecursive: true, // Enables deep folder/file creation inside this folder such as /images/deep/nested/my-image.png
        },
        
        //add as many more rules as you want
    },
});
```

# Notes

All dot files such as `.gitignore` and `node_modules` are ignored by default
